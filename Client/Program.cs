﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net; // Để sử dụng lớp IPAddress và IPEndPoint
using System.Net.Sockets; // để sử dụng lớp Socket

namespace Client
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                // yêu cầu người dùng nhập IP của server
                Console.Title = "Udp Client";
                Console.Write("Server IP Address: ");
                string serverIpStr = Console.ReadLine();
                // Chuyển đổi chuỗi kí tự thành Object thuộc kiểu IPAddress
                IPAddress serverIp = IPAddress.Parse(serverIpStr);

                // Yêu cầu người dùng nhập cổng của server
                Console.Write("Server port: ");
                string serverPortStr = Console.ReadLine();
                int serverPort = int.Parse(serverPortStr);

                // Đây là địa chỉ của tiến trình server trên mạng
                // mỗi endpoint chứa ip của host và port của tiến trình
                IPEndPoint ipep = new IPEndPoint(serverIp, serverPort);

                int size = 1024; // kích thước của bộ đệm
                byte[] receiveBuffer = new byte[size]; // mảng byte làm bộ đệm

                while (true)
                {
                    // Yêu cầu người dùng nhập một chuỗi
                    Console.ForegroundColor = ConsoleColor.Green;
                    Console.Write("$ Text >> ");
                    Console.ResetColor();
                    string text = Console.ReadLine();
                    if (text.ToUpper().Equals("EXIT")) break;
                    // khởi tạo object của lớp socket để sử dụng dịch vụ Udp
                    // Lưu ý SocketType của Udp là Dgram (datagram)
                    Socket socket = new Socket(SocketType.Dgram, ProtocolType.Udp);
                    // Biến đổi chuỗi thành mảng byte
                    byte[] sendBuffer = Encoding.ASCII.GetBytes(text);
                    // Gửi mảng byte trên đến tiến trình server
                    socket.SendTo(sendBuffer, ipep);
                    // EndPoint này chỉ dùng khi nhận dữ liệu
                    EndPoint dummyEndPoint = new IPEndPoint(IPAddress.Any, 0);
                    // Nhận mảng byte từ dịch vụ Udp và lưu vào bộ đệm
                    // Biến dummyEndPoint có nhiệm vụ lưu lại địa chỉ của tiến trình nguồn
                    // Tuy nhiên, ở đây chúng ta đã biết tiến trình nguồn là Server
                    // do đó, dummyEndPoint không có giá trị sử dụng
                    int length = socket.ReceiveFrom(receiveBuffer, ref dummyEndPoint);
                    // chuyển đổi mảng byte về chuỗi
                    string result = Encoding.ASCII.GetString(receiveBuffer, 0, length);
                    // Xóa bộ đệm để lần sau sử dụng cho yên tâm
                    Array.Clear(receiveBuffer, 0, size);
                    // Đóng Socket và giải phóng tài nguyên
                    socket.Close();
                    // in kết quả ra màn hình
                    Console.WriteLine($"$ >> {result}");
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Lỗi : "+ ex.Message);
            }
        }
    }
}
