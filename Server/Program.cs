﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;

namespace Server
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Title = "Udp Server";

            // Giá trị Any của IPAddress tương ứng với Ip của tất cả giao diện trên mạng
            IPAddress localIP = IPAddress.Any;
            // Tiến trình Server sẽ sử dụng cổng 1308
            int localPort = 1308;
            // Biến này sẽ chứa địa chỉ của tiến trình server trên mạng
            IPEndPoint ipep = new IPEndPoint(localIP, localPort);
            // Yếu cầu hệ điều hành cho phép chiếm dụng cổng 1308
            // Server sẽ nghe trên tất cả các mạng mà máy tính này kết nối tới
            // chỉ cần gói tin Udp đến cổng 1308, tiến trình server sẽ nhận được

            // Một overload khác của hàm tạo Socket
            // InterNetWork là họ địa chỉ dành cho IPv4
            Socket socket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
            socket.Bind(ipep);
            Console.WriteLine($"Local socket bind to {ipep}. Waiting for request ...");

            int size = 1024;
            byte[] receiveBuffer = new byte[size];
            while (true)
            {
                // Biến này về sau sẽ chứa địa chỉ của tiến trình client nào gửi gói tin tới
                EndPoint remoteEndPoint = new IPEndPoint(IPAddress.Any, 0);
                // Khi nhận được gói tin nào sẽ lưu lại địa chỉ của tiến trình client
                int length = socket.ReceiveFrom(receiveBuffer, ref remoteEndPoint);
                string text = Encoding.ASCII.GetString(receiveBuffer, 0, length);
                Console.WriteLine($"Receive from {remoteEndPoint} : {text}");

                // Chuyển chuỗi thành dạng in hoa
                string result = text.ToUpper();

                byte[] sendBuffer = Encoding.ASCII.GetBytes(result);

                // Gủi kết quả lại cho client
                socket.SendTo(sendBuffer, remoteEndPoint);
                Array.Clear(receiveBuffer, 0, size);

            }

        }
    }
}
